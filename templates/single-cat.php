<?php get_header();?>

<?php if(have_posts()): while(have_posts()): the_post(); ?>
<article class="cat" id="cat-<?php echo get_the_id(); ?>">  
	<?php include('template-cat.php'); ?>
</article>
<?php endwhile; endif; ?>

<?php get_footer();?>