<?php get_header();?>

<header class="cat">
	<h1><?php Cat::title(); ?></h1>
	<?php Cat::description(); ?>
</header>
<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<?php while (have_posts()) : the_post(); ?>
<article class="cat archive" id="cat-<?php echo get_the_id(); ?>">  
	<?php include('template-cat.php'); ?>
</article>
<?php endwhile; ?>


<?php get_footer();?>