<?php
/**
 * Cat Post Type
 *
 * @package   Cat_Post_Type
 * @license   GPL-2.0+
 */

/**
 * Helper functions for the cat post type.
 *
 * @package Cat_Post_Type
 */
class Cat {

	/**
	 * Returns cat name and adds link 
	 * if this is not a single post.
	 * @return string Formatted html
	 */
	public static function cat_name() {
		global $post;
		$cat_name = get_the_title();
		// Format for archive or single
		$cat_name = ( is_single() ? '<h1>'.$cat_name.'</h1>' : self::link_cat('<h2>' . $cat_name . '</h2>', 'cat_title') );		
		return $cat_name;
	}

	/**
	 * Echoes the cat name
	 * @return void
	 */
	public static function name() {
		echo self::cat_name();
	}

	/**
	 * Returns cat image html and adds link 
	 * if this is not a single post.
	 * 
	 * @return string Formatted html.
	 */
	public static function cat_img() {
		global $post;
		$url= wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
		$img = '<img src="'.$url.'" class="cat img img-responsive img-rounded">';
		
		return ( is_single() ? $img : self::link_cat($img, 'cat_img'));
	}
	public static function link_cat($html, $class='', $url=null) {
		global $post;
		if( ! $url ) $url = get_permalink();
		return '<a href="' . $url . '" title="Meet ' . get_the_title() . '!" class="cat '.$class.'">' . $html . '</a>';
	} 
	public static function get_display_title() {
		return get_option('cc_cat_display_title');
	}

	public static function get_display_description() {
		return get_option('cc_cat_display_description');
	}


	public static function title() {
		echo self::get_display_title();
	}
	public static function description() {
		echo self::get_display_description();
	}

	/**
 	* Echoes the cat image
 	* @return void
 	*/
	public static function img() {
		echo self::cat_img();
	}


}
