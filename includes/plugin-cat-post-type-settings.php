<?php

if ( file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/cmb2/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/CMB2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/CMB2/init.php';
}

class Cat_Settings {

	/**
	 * Adds the settings menu to the Cat post menu.
	 */
	public static function settings_menu() {
	    add_submenu_page('edit.php?post_type=cat-cafe-cat', 
	    	'Cat Settings', 
	    	'Settings', 
	    	'edit_posts', 
	    	basename(__FILE__), 
	    	['cat_settings','settings_page']
	    );
	}
	function register_metabox() {
		$prefix = 'cc_cat_';
		$cmb_page = new_cmb2_box([ 
			'id' => $prefix.'settings',
			'title' => __( 'Cat Display Settings', 'cmb2'),
			'object_types' => ['post'],
			'show_name' => true,
		]);
		$cmb_page->add_field( array(
			'name'       => __( 'Test Text', 'cmb2' ),
			'desc'       => __( 'field description (optional)', 'cmb2' ),
			'id'         => $prefix . 'text',
			'type'       => 'text',
			// 'show_on_cb' => 'cc_hide_if_no_cats', // function should return a bool value
			// 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
			// 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
			// 'on_front'        => false, // Optionally designate a field to wp-admin only
			// 'repeatable'      => true,
		) );

		$cmb_page->add_field( array(
			'name' => __( 'Test Image', 'cmb2' ),
			'desc' => __( 'Upload an image or enter a URL.', 'cmb2' ),
			'id'   => $prefix . 'image',
			'type' => 'file',
		) );
	}
	/**
	 * Registers settings for the FAQ post type.
	 */
	public static function settings_register() {
		register_setting( 'cc_cat_settings_group', 'cc_cat_display_title'); 
		register_setting( 'cc_cat_settings_group', 'cc_cat_display_description'); 
		// register_setting( 'cc_cat_settings_group', 'cc_faq_privacy_policy'); 
		// register_setting( 'cc_cat_settings_group', 'cc_faq_shipping_policy'); 
	}

	/**
	 * Creates a wp_editor formatted for the FAQ settings page.
	 * @param  string $id 	The id of the option you want to 
	 *                     	assign to the editor.
	 * @return wp_editor    TinyMCE Text Editor Instance.
	 */
	public static function  editor($id) {
		$editor_settings = [
			'textarea_rows' => 10,
			'media_buttons' => false,
			'textarea_name' =>$id,
		];	
		wp_editor( get_option($id) , $id, $editor_settings );
	}

	/**
	 * Defines the admin settings page and form inputs.
	 * @return void
	 */
	function settings_page() {
		?>
		<h2>Cat Settings</h2>
		<p style="font-size:1.5em"> You can change the title and description shown on the Cats page by updating this form with your new content. <br> This can be viewed at: <br> <a href="/cats/"><?php echo site_url();?>/cats/</a></p>
		<form method="post" action="options.php" style="width:98%;"> 
			<?php 
				settings_fields( 'cc_cat_settings_group' ); 
				do_settings_sections( 'cc_cat_settings_group' );
			?>
			<table class="form-table" style="max-width: 85%;" border="0">
				<tr>
					<th style="width: 20%;">Display Title</th>
					<td>
						<input type="text" name="cc_cat_display_title" value="<?php echo esc_attr( get_option('cc_cat_display_title') ); ?>" style="width:100%" >
					</td>
				</tr>
				<tr style="height: 1em;">
					<th>Cat Page Description</th>
					<td rowspan="2"> <?php self::editor('cc_cat_display_description'); ?> </td>
				</tr>
				<tr> <td style="vertical-align:top;"> Add some descriptive text to be displayed on the Cats page. </td> </tr>
			</table>
			<?php submit_button(); ?>
		</form>
		<?php
	}

}
add_action( 'cmb2_admin_init', ['Cat_Settings', 'register_metabox'] );

add_action( 'admin_init', ['cat_settings', 'settings_register'] );
add_action('admin_menu' , ['cat_settings', 'settings_menu']); 


