<?php
/**
 * Cat Post Type
 *
 * @package   Cat_Post_Type
 * @license   GPL-2.0+
 */

/**
 * Register post types and taxonomies.
 *
 * @package Cat_Post_Type
 */
class Cat_Post_Type_Registrations {

	public $post_type = 'cat-cafe-cat';

	public $taxonomies = array( 'cat-category' );

	public function init() {
		// Add the team post type and taxonomies
		add_action( 'init', array( $this, 'register' ) );
	}

	/**
	 * Initiate registrations of post type and taxonomies.
	 *
	 * @uses Cat_Post_Type_Registrations::register_post_type()
	 */
	public function register() {
		$this->register_post_type();
	}

	/**
	 * Register the custom post type.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/register_post_type
	 */
	protected function register_post_type() {
		$labels = array(
			'name'               => __( 'Cats', 'cat-cafe-cat' ),
			'singular_name'      => __( 'Cat', 'cat-cafe-cat' ),
			'add_new'            => __( 'Add Cat', 'cat-cafe-cat' ),
			'add_new_item'       => __( 'Add Cat', 'cat-cafe-cat' ),
			'edit_item'          => __( 'Edit Cat', 'cat-cafe-cat' ),
			'new_item'           => __( 'New Cat', 'cat-cafe-cat' ),
			'view_item'          => __( 'View Cat', 'cat-cafe-cat' ),
			'search_items'       => __( 'Search Cats', 'cat-cafe-cat' ),
			'not_found'          => __( 'No cats found', 'cat-cafe-cat' ),
			'not_found_in_trash' => __( 'No cats in the trash', 'cat-cafe-cat' ),
		);

		$supports = array(
			'title',
			'editor',
			'thumbnail',
			'custom-fields',
			'revisions',
		);

		$args = array(
			'labels'            => $labels,
			'supports'          => $supports,
			'public'            => true,
			'has_archive'       => true,
			'capability_type'   => 'post',
			'rewrite'           => array( 'slug' => 'cats', 'with_front' => false,), // Permalinks format
			'menu_position'     => 20,
			'menu_icon'         => 'dashicons-id',
			'show_in_nav_menus' => true,
			'show_in_menu'      => true,
		);

		$args = apply_filters( 'cat_post_type_args', $args );

		register_post_type( $this->post_type, $args );
	}
}