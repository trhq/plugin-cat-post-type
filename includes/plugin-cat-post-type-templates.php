<?php
/**
 * Cat Post Type
 *
 * @package   Faq_Post_Type
 * @license   GPL-2.0+
 */

/**
 * Register post types and taxonomies.
 *
 * @package Faq_Post_Type
 */
class Cat_Post_Type_Templates {

	public static function path() {
		return plugin_dir_path( __FILE__ ) . '..' . '/templates/';
	}

	public function __construct() {

	}

	/**
	 * Adds Filter for single cat template.
	 */
	public static function single($template) {
	    global $post;
	    // Is this a "cat" post?
	    if ($post->post_type == "cat-cafe-cat"){

	        $template_path = self::path() . 'single-cat.php';
	        // Return the custom template
	        if(file_exists($template_path)) return $template_path;
	    }

	    // Otherwise do nothing
	    return $template;
	}

	/**
	 * Add Filter for single cat template.
	 */
	public static function archive($template) {
	    global $post;

	    // Is this a "cat" post?
	    if ($post->post_type == "cat-cafe-cat"){
	    	$template_path = self::path() . 'archive-cat.php';

	        // Return the custom template
	        if(file_exists($template_path)) return $template_path;
	    }

	    // Otherwise, do nothing.
	    return $template;
	}

	/**
	 * Increases the number of cats to display on the archive.
	 * @param  WP_Query $query The Wordpress Query
	 * @return void        
	 */
	public static function archive_size( $query ) {
	    if ( is_post_type_archive( 'cat-cafe-cat' ) ) {
	        // Display 50 posts for a custom post type called 'movie'
	        $query->set( 'posts_per_page', 50 );
	        $query->set( 'orderby', 'title' );
			$query->set( 'order', 'ASC' );
	        return;
	    }
	}
}

add_filter('single_template', ['Cat_Post_Type_Templates', 'single']);
add_filter('archive_template', ['Cat_Post_Type_Templates', 'archive']);
add_action( 'pre_get_posts', ['Cat_Post_Type_Templates','archive_size'], 1 );
